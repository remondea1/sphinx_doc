# A basic Python project

This repo shows the architecure of a basic Python project, with documentation and testing.
This repo demonstrate also the usage of Sphinx for Python documentation, as well as **pytest** for the testing of your Python code.

## I. This project demonstrastion

### 1. Install Sphinx

In the root folder, run 

```console
pip install -r requirements.txt
```

### 2. Build the documentation

In the *docs/* folder, run

```console
make html 
```

### 3. Consult the documentation

The html documentation is stored in *docs/build/html/*. Open index.html with your navigator

![Alt text](/commons/sphinx.png?raw=true "Sphinx preview")

### 4. Run pytest

To see all the tests in your project, run this at the root folder :
```console
pytest --cov=src/ 
```

![Alt text](/commons/pytest.png?raw=true "Pytest coverage result")

It shows that all the tests have succeeded, and that your test cover 100% of the code.

# II. Use in your own project

### 1. Copy the docs folder

Copy the *docs/* folder at the root of your project.
**Remove the *docs/source/_autosummary* and *docs/build* folder if they exist.**

### 2. Install the packages

Add the packages win requirements.txt to your own requirements.txt and use 

```console
pip install -r requirements.txt
```


### 3. Run sphinx and pytest

You source files should be put in a *src/* folder and your tests in a */tests* folder.

In the *docs/* folder, run

Run 
```console
make clean
make html 
```

This creates the documentation under docs/build/html

For pytest, run this at the root of your project folder
```console
pytest --cov=src/
```
