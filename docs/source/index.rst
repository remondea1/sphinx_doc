Welcome to Foo's documentation!
=======================================

This is a demonstration of Sphinx.



.. autosummary::
   :toctree: _autosummary
   :template: custom-module-template.rst
   :recursive:

   src


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
