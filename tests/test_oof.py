from src.utils.oof import Ooof, oof_function


def test_Oof():
    """Test the Oof class"""

    instance = Ooof("abc", 10)

    # Class instantiate correctly
    assert instance.foo, instance.bar == ("abc", 10)


def test_concatenate_string():
    """Test of the concatenate method."""

    instance = Ooof("abc", 10)

    assert (
        instance.concatenate_strings(["welcome", "to", "pvlab"]) == "welcome to pvlab"
    )
    assert (
        instance.concatenate_strings(["welcome", "to", "pvlab"], ",")
        == "welcome,to,pvlab"
    )


def test_oof_function():
    """Test of the oof function"""

    assert oof_function() == [10, "azerty"]
