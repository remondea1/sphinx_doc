from src.utils.foo_package.foo_module import foo_function, idle


def test_foo_function():
    """Test the foo_function function."""
    assert foo_function(1, "a") == [1, "a"]


def test_idle():
    """Test the idle function."""
    assert idle() == None
