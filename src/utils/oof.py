"""oof.py:  functions

This module contains functions and classes to do stuff too !
"""

from typing import List, Union
from .foo_package.foo_module import foo_function


class Ooof:
    """A cool class.

    Attributes
    ----------
    foo: str
        An attribute of the class.
    bar: int
        And another one.

    Parameters
    ----------
    foo: str
        The parameter

    Methods
    -------
    concatenate_strings:
        Concatenate strings.

    """

    foo: str
    bar: int

    def __init__(self, foo: str, bar: int):
        """Class constructor.

        It creates an instance of the class.

        Parameters
        ----------
        foo : str
            The parameter.
        bar: int
            Another one.

        """
        self.foo = foo
        self.bar = bar

    def concatenate_strings(self, data: List[str], sep: str = " ") -> str:
        """This function does something.

        Parameters
        ----------
        data : List[str]
            The list of strings to concatenate

        Returns
        -------
        List[str]
            The list of strings to concatenate.

        Example
        -------
        >>> concatenate_strings(['concatenate', 'me'])
        'concatenate me'


        """
        return sep.join(data)


def oof_function() -> List[Union[int, str]]:
    """Run the foo.foo function for set parameters.

    This function calls the :func:`~.foo_package.foo_module.foo_function` function.

    Returns
    -------
    List[Union[int, str]]
        The output list of parameters.

    Examples
    --------
    >>> oof()
    [10, 'azerty']
    """

    return foo_function(10, "azerty")
