"""foo_module.py:  functions

This module includes functions to do stuff.
"""


from typing import List, Union


def foo_function(number: int, string: str) -> List[Union[int, str]]:
    """Foo function.

    Parameters
    ----------
    number : int
        The 1st parameter.
    string : str
        The second parameter.

    Returns
    -------
    List[Union[int, str]]
        The output list of parameters.

    Examples
    --------
    >>> foo(1, 'abc')
    [1, 'abc']

    """
    return [number, string]


def idle() -> None:
    """This function does nothing"""
